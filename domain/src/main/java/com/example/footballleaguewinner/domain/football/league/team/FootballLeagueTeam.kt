package com.example.footballleaguewinner.domain.football.league.team

data class FootballLeagueTeam(val name: String, val players: List<Player>) {
    init {
        require(players.size >= MIN_PLAYER_LIST_SIZE) {
            "The player list needs to contains at least $MIN_PLAYER_LIST_SIZE players"
        }
    }

    private companion object {
        const val MIN_PLAYER_LIST_SIZE = 12
    }

    data class Player(val name: String, val position: String)
}
