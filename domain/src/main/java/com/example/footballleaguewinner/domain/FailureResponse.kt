package com.example.footballleaguewinner.domain

sealed class FailureResponse<T : Any>(val reason: T?)
class GenericFailureResponse(reason: String) : FailureResponse<String>(reason = reason)
class UnknownFailureResponse(reason: Throwable) : FailureResponse<Throwable>(reason = reason)
