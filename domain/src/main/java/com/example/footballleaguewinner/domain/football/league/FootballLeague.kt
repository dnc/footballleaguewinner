package com.example.footballleaguewinner.domain.football.league

data class FootballLeague(val name: String, val emblemUrl: String)
