package com.example.footballleaguewinner.domain.football.league

import com.example.footballleaguewinner.domain.EntityId
import java.time.LocalDateTime

data class FootballLeagueMatches(
    val leagueName: String,
    val matches: List<Match>
) {
    data class Match(val time: LocalDateTime, val winningTeam: Team) {
        data class Team(val id: EntityId, val name: String)
    }
}
