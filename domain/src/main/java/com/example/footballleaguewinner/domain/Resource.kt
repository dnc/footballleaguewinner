package com.example.footballleaguewinner.domain

class Resource<out T : Any?> private constructor(
    private val successResponse: T? = null,
    private val failureResponse: FailureResponse<*>? = null,
) {
    init {
        require(listOfNotNull(successResponse, failureResponse).size == 1) { "Expected one non-null constructor parameter" }
    }

    fun requireSuccess(): T {
        return requireNotNull(successResponse)
    }

    fun requireFailure(): FailureResponse<*> {
        return requireNotNull(failureResponse)
    }

    val isSuccessful get() = successResponse != null

    val isFailed get() = failureResponse != null

    companion object {
        fun <T : Any?> success(successResponse: T): Resource<T> {
            return Resource<T>(successResponse = successResponse)
        }

        fun <T : Any?> failure(failureResponse: FailureResponse<*>): Resource<T> {
            return Resource<T>(failureResponse = failureResponse)
        }
    }
}
