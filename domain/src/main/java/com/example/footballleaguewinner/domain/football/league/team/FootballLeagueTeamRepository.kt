package com.example.footballleaguewinner.domain.football.league.team

import com.example.footballleaguewinner.domain.EntityId
import com.example.footballleaguewinner.domain.Resource

/**
 * Repository of methods that return [football league team][FootballLeagueTeam] related data
 */
interface FootballLeagueTeamRepository {
    /**
     * Returns a [FootballLeagueTeam] instance for the given [football league team id][footballLeagueTeamId]
     */
    suspend fun getFootballLeagueTeamById(footballLeagueTeamId: EntityId): Resource<FootballLeagueTeam>
}
