package com.example.footballleaguewinner.domain.football.league.match

import com.example.footballleaguewinner.domain.EntityId
import com.example.footballleaguewinner.domain.Resource
import com.example.footballleaguewinner.domain.football.league.FootballLeague
import com.example.footballleaguewinner.domain.football.league.FootballLeagueMatches
import java.time.LocalDate

/**
 * Repository of methods that return football league and league match related data
 */
interface FootballLeagueRepository {
    /**
     * Returns a [FootballLeague] instance for the given [id][footballLeagueId]
     */
    suspend fun getLeagueById(footballLeagueId: EntityId): Resource<FootballLeague>

    /**
     * Returns a [FootballLeagueMatches] instance for the [FootballLeague] with the given [id][footballLeagueId],
     * containing all the [matches][FootballLeagueMatches.Match] that occur within the specified match date range.
     */
    suspend fun getFinishedMatchesByLeagueId(
        footballLeagueId: EntityId,
        matchStartDate: LocalDate,
        matchEndDate: LocalDate
    ): Resource<FootballLeagueMatches>

    suspend fun getLastFinishedMatchInLatestSeasonByLeagueId(
        footballLeagueId: EntityId
    ): Resource<FootballLeagueMatches.Match?>
}
