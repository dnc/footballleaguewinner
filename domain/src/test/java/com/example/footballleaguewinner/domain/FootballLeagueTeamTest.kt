package com.example.footballleaguewinner.domain

import com.example.footballleaguewinner.domain.football.league.team.FootballLeagueTeam
import org.junit.Test

class FootballLeagueTeamTest {
    @Test(expected = IllegalArgumentException::class)
    fun whenInitializedWithEmptyPlayerList_shouldThrowException() {
        FootballLeagueTeam(name = "Real Madrid", players = emptyList())
    }

    @Test(expected = IllegalArgumentException::class)
    fun whenInitializedWith10Players_shouldThrowException() {
        FootballLeagueTeam(name = "Real Madrid", players = (1..10).map { FootballLeagueTeam.Player(name = it.toString(), position = "Goalkeeper") })
    }

    @Test
    fun whenInitializedWith12Players_shouldNotThrowException() {
        FootballLeagueTeam(name = "FC Barcelona", players = (1..12).map { FootballLeagueTeam.Player(name = it.toString(), position = "Goalkeeper") })
    }
}
