package com.example.footballleaguewinner.data.di

import com.example.footballleaguewinner.data.BuildConfig
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber

@Module
@InstallIn(SingletonComponent::class)
object RetrofitModule {
    @Provides
    fun provideRootRetrofit(moshi: Moshi): Retrofit {
        return createRetrofit(moshi = moshi, baseUrl = BuildConfig.BASE_API_URL)
    }

    private fun createRetrofit(
        moshi: Moshi,
        baseUrl: String
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(createOkHttpClient())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    private fun createOkHttpClient(): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()
            .addInterceptor(createHttpLoggingInterceptor())
            .addInterceptor(createRequestInterceptor())
        return okHttpClientBuilder.build()
    }

    private fun createHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor(logger = { message -> Timber.d(message) }).apply {
            level = if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.BASIC
            }
        }
    }

    private fun createRequestInterceptor(): Interceptor {
        return Interceptor { chain ->
            var request = chain.request()

            val headers = request.headers.newBuilder()
                .add("X-Auth-Token", BuildConfig.API_KEY)
                .build()

            request = request.newBuilder()
                .headers(headers)
                .build()

            chain.proceed(request)
        }
    }
}
