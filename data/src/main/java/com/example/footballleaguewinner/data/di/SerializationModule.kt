package com.example.footballleaguewinner.data.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object SerializationModule {
    @Provides
    internal fun provideMoshi(): Moshi {
        // TODO: use codegen
        return Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    }
}
