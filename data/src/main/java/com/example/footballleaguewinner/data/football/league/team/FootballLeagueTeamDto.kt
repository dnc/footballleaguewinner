package com.example.footballleaguewinner.data.football.league.team

import com.squareup.moshi.Json

data class FootballTeamLeagueDto(
    @Json(name = "name") val name: String,
    @Json(name = "crestUrl") val crestUrl: String,
    @Json(name = "squad") val members: List<Member>
) {
    data class Member(
        @Json(name = "name") val name: String,
        @Json(name = "position") val position: String?, // Null for non-players
        @Json(name = "role") private val rawType: String
    ) {
        val type = enumValueOf<Type>(rawType)

        enum class Type {
            PLAYER, COACH, INTERIM_COACH, ASSISTANT_COACH
        }
    }
}
