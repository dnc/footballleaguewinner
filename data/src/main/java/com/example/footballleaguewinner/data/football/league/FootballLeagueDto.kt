package com.example.footballleaguewinner.data.football.league

import com.squareup.moshi.Json

internal data class FootballLeagueDto(
    @Json(name = "name") val name: String,
    @Json(name = "emblemUrl") val emblemUrl: String
)
