package com.example.footballleaguewinner.data.response

import com.squareup.moshi.Json

internal data class FailureResponseDto(
    @Json(name = "errorCode") val errorCode: Int,
    @Json(name = "message") val message: String
)
