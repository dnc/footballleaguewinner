package com.example.footballleaguewinner.data.football.league.team

import com.example.footballleaguewinner.domain.EntityId
import retrofit2.http.GET
import retrofit2.http.Path

internal interface FootballLeagueTeamService {
    @GET("teams/{id}")
    suspend fun getFootballLeagueTeamById(@Path("id") teamId: EntityId): FootballTeamLeagueDto
}
