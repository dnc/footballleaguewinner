package com.example.footballleaguewinner.data.football.league.team

import com.example.footballleaguewinner.data.request.Request
import com.example.footballleaguewinner.domain.EntityId
import com.example.footballleaguewinner.domain.Resource
import com.example.footballleaguewinner.domain.football.league.team.FootballLeagueTeam
import com.example.footballleaguewinner.domain.football.league.team.FootballLeagueTeamRepository
import com.squareup.moshi.Moshi
import timber.log.Timber
import javax.inject.Inject

internal class FootballLeagueTeamRepositoryImpl @Inject constructor(
    private val moshi: Moshi,
    private val footballLeagueTeamService: FootballLeagueTeamService
) : FootballLeagueTeamRepository {
    override suspend fun getFootballLeagueTeamById(footballLeagueTeamId: EntityId): Resource<FootballLeagueTeam> {
        return Request(
            moshi = moshi,
            service = {
                footballLeagueTeamService.getFootballLeagueTeamById(teamId = footballLeagueTeamId).run {
                    // TODO: refactor mapper to separate class, test mapper
                    FootballLeagueTeam(
                        name = name,
                        players = members.filter { it.type == FootballTeamLeagueDto.Member.Type.PLAYER }.map {
                            FootballLeagueTeam.Player(name = it.name, position = requireNotNull(it.position))
                        }.also { Timber.d("Loaded ${it.size} players") }
                    ).also { Timber.d("Loaded team '$name'") }
                }
            }
        ).make()
    }
}
