package com.example.footballleaguewinner.data.di

import com.example.footballleaguewinner.data.football.league.FootballLeagueRepositoryImpl
import com.example.footballleaguewinner.data.football.league.team.FootballLeagueTeamRepositoryImpl
import com.example.footballleaguewinner.domain.football.league.match.FootballLeagueRepository
import com.example.footballleaguewinner.domain.football.league.team.FootballLeagueTeamRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class RepositoryModule {
    @Binds
    internal abstract fun bindFootballLeagueRepository(
        footballLeagueRepositoryImpl: FootballLeagueRepositoryImpl
    ): FootballLeagueRepository

    @Binds
    internal abstract fun bindFootballLeagueTeamRepository(
        footballLeagueTeamRepository: FootballLeagueTeamRepositoryImpl
    ): FootballLeagueTeamRepository
}
