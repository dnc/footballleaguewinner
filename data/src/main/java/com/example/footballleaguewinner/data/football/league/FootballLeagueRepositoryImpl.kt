package com.example.footballleaguewinner.data.football.league

import com.example.footballleaguewinner.data.football.league.FootballLeagueMatchesDto.MatchDto.WinnerType
import com.example.footballleaguewinner.data.request.Request
import com.example.footballleaguewinner.domain.EntityId
import com.example.footballleaguewinner.domain.Resource
import com.example.footballleaguewinner.domain.football.league.FootballLeague
import com.example.footballleaguewinner.domain.football.league.FootballLeagueMatches
import com.example.footballleaguewinner.domain.football.league.match.FootballLeagueRepository
import com.squareup.moshi.Moshi
import timber.log.Timber
import java.time.LocalDate
import javax.inject.Inject

internal class FootballLeagueRepositoryImpl @Inject constructor(
    private val moshi: Moshi,
    private val footballLeagueService: FootballLeagueService
) : FootballLeagueRepository {
    override suspend fun getLeagueById(footballLeagueId: EntityId): Resource<FootballLeague> {
        return Request(
            moshi = moshi,
            service = {
                footballLeagueService.getLeagueById(footballLeagueId = footballLeagueId).run {
                    // TODO: refactor mapper to separate class, test mapper
                    FootballLeague(name = name, emblemUrl = emblemUrl)
                }
            }
        ).make()
    }

    override suspend fun getFinishedMatchesByLeagueId(
        footballLeagueId: EntityId,
        matchStartDate: LocalDate,
        matchEndDate: LocalDate
    ): Resource<FootballLeagueMatches> {
        Timber.d("Loading finished matches for league with id=$footballLeagueId; ($matchStartDate to $matchEndDate)")

        return Request(
            moshi = moshi,
            service = {
                getFinishedMatchesByLeagueIdImpl(
                    footballLeagueId = footballLeagueId,
                    matchStartDate = matchStartDate,
                    matchEndDate = matchEndDate
                )
            }
        ).make()
    }

    override suspend fun getLastFinishedMatchInLatestSeasonByLeagueId(footballLeagueId: EntityId): Resource<FootballLeagueMatches.Match?> {
        return Request(
            moshi = moshi,
            service = {
                getFinishedMatchesByLeagueIdImpl(
                    footballLeagueId = footballLeagueId,
                    matchStartDate = null,
                    matchEndDate = null
                ).matches.maxByOrNull { match -> match.time }
            }
        ).make()
    }

    private suspend fun getFinishedMatchesByLeagueIdImpl(
        footballLeagueId: EntityId,
        matchStartDate: LocalDate? = null,
        matchEndDate: LocalDate? = null
    ): FootballLeagueMatches {
        return footballLeagueService.getFinishedMatchesByLeagueId(
            footballLeagueId = footballLeagueId,
            matchStartDate = matchStartDate?.let { FootballLeagueService.dateFormatter.format(it) },
            matchEndDate = matchEndDate?.let { FootballLeagueService.dateFormatter.format(it) }
        ).run {
            // TODO: refactor mapper to separate class, test mapper
            FootballLeagueMatches(
                leagueName = league.name,
                matches = matches.filter { it.winnerType != WinnerType.DRAW && it.winnerType != null }.map { matchDto ->
                    FootballLeagueMatches.Match(
                        time = matchDto.time,
                        winningTeam = when (matchDto.winnerType) {
                            WinnerType.HOME_TEAM -> matchDto.homeTeam
                            WinnerType.AWAY_TEAM -> matchDto.awayTeam
                            else -> error("WinnerType '${matchDto.winnerType}' is not supported")
                        }.run { FootballLeagueMatches.Match.Team(id = id, name = name) }
                    )
                }.also { Timber.d("Loaded ${it.size} matches") }
            )
        }
    }
}
