package com.example.footballleaguewinner.data.football.league

import com.example.footballleaguewinner.domain.EntityId
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.time.format.DateTimeFormatter

internal interface FootballLeagueService {
    @GET("competitions/{id}")
    suspend fun getLeagueById(@Path("id") footballLeagueId: EntityId): FootballLeagueDto

    @GET("competitions/{id}/matches?status=FINISHED")
    /**
     * Returns the finished matches for the league with the given [id][footballLeagueId]. If [matchStartDate] and
     * [matchEndDate] are not specified, it returns the finished matches from the latest season
     */
    suspend fun getFinishedMatchesByLeagueId(
        @Path("id") footballLeagueId: EntityId,
        @Query("dateFrom") matchStartDate: String?,
        @Query("dateTo") matchEndDate: String?
    ): FootballLeagueMatchesDto

    companion object {
        val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    }
}
