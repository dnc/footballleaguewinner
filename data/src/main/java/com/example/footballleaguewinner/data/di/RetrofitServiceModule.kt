package com.example.footballleaguewinner.data.di

import com.example.footballleaguewinner.data.football.league.FootballLeagueService
import com.example.footballleaguewinner.data.football.league.team.FootballLeagueTeamService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import retrofit2.Retrofit
import retrofit2.create

@Module
@InstallIn(ViewModelComponent::class)
object RetrofitServiceModule {
    @Provides
    internal fun provideFootballLeagueService(retrofit: Retrofit): FootballLeagueService = retrofit.create()

    @Provides
    internal fun provideFootballLeagueTeamService(retrofit: Retrofit): FootballLeagueTeamService = retrofit.create()
}
