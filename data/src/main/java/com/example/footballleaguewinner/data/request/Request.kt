package com.example.footballleaguewinner.data.request

import com.example.footballleaguewinner.data.response.FailureResponseDto
import com.example.footballleaguewinner.domain.FailureResponse
import com.example.footballleaguewinner.domain.GenericFailureResponse
import com.example.footballleaguewinner.domain.Resource
import com.example.footballleaguewinner.domain.UnknownFailureResponse
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import retrofit2.Response
import timber.log.Timber

internal class Request<Rsuccess : Any?> constructor(
    private val moshi: Moshi,
    private val service: suspend () -> Rsuccess
) {
    suspend fun make(): Resource<Rsuccess> {
        // TODO: inject dispatcher via DI
        return withContext(Dispatchers.IO) {
            try {
                val successResponse = service()
                ensureActive()
                Resource.success(successResponse)
            } catch (e: HttpException) {
                Timber.e(e, "Caught HTTP error while processing request")

                val response = e.response()

                try {
                    val genericFailureResponse = response.mapErrorBody()
                    Timber.d("Successfully mapped response error body")
                    Resource.failure(genericFailureResponse)
                } catch (e: Exception) {
                    Timber.e(e, "Caught exception while mapping response error body")
                    // TODO: add more specific FailureResponse implementation
                    Resource.failure(UnknownFailureResponse(reason = e))
                }
            } catch (e: Exception) {
                Timber.e(e, "Caught exception while processing request")
                Resource.failure(UnknownFailureResponse(reason = e))
            }
        }
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    private suspend fun Response<*>?.mapErrorBody(): FailureResponse<String> {
        val response = this

        // TODO: inject dispatcher via DI
        return withContext(Dispatchers.IO) {
            val rawErrorBody = response?.errorBody()?.string() ?: "".also { Timber.w("Response error body is empty") }

            moshi.adapter(FailureResponseDto::class.java).fromJson(rawErrorBody)?.toFailureResponse()
                ?: GenericFailureResponse(reason = rawErrorBody)
        }
    }

    private fun FailureResponseDto.toFailureResponse() = GenericFailureResponse(reason = message)
}
