package com.example.footballleaguewinner.data.football.league
import com.example.footballleaguewinner.domain.EntityId
import com.squareup.moshi.Json
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

internal data class FootballLeagueMatchesDto(
    @Json(name = "competition") val league: LeagueDto,
    @Json(name = "matches") val matches: List<MatchDto>
) {
    data class LeagueDto(
        @Json(name = "id") val id: EntityId,
        @Json(name = "name") val name: String
    )

    data class MatchDto(
        @Json(name = "awayTeam") val awayTeam: TeamDto,
        @Json(name = "homeTeam") val homeTeam: TeamDto,
        @Json(name = "utcDate") private val utcDate: String,
        @Json(name = "score") private val score: ScoreDto
    ) {
        val winnerType: WinnerType? = score.winnerType
        val time: LocalDateTime = LocalDateTime.parse(utcDate, dateFormatter)

        private companion object {
            val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
        }

        data class TeamDto(
            @Json(name = "id") val id: EntityId,
            @Json(name = "name") val name: String
        )

        data class ScoreDto(
            @Json(name = "winner") private val rawWinnerType: String? // This can be null, even for finished matches
        ) {
            val winnerType: WinnerType? = rawWinnerType?.let { enumValueOf<WinnerType>(it) }
        }

        enum class WinnerType {
            HOME_TEAM,
            AWAY_TEAM,
            DRAW
        }
    }
}
