package com.example.footballleaguewinner.data.football.league.team

import com.example.footballleaguewinner.domain.football.league.team.FootballLeagueTeam
import com.google.common.truth.Truth
import com.squareup.moshi.Moshi
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito

class FootballLeagueTeamRepositoryImplTest {
    @Test
    fun whenReturningFootballLeagueTeam_shouldMapCorrectly() = runBlocking<Unit> {
        // given
        val footballLeagueTeamService = Mockito.mock(FootballLeagueTeamService::class.java)
        Mockito.`when`(footballLeagueTeamService.getFootballLeagueTeamById(teamId = anyLong())).thenReturn(
            fakeFootballLeagueTeamDto
        )

        val footballLeagueTeamRepository = FootballLeagueTeamRepositoryImpl(
            moshi = Moshi.Builder().build(), // Cannot be mocked via Mockito as it's a final class
            footballLeagueTeamService = footballLeagueTeamService
        )

        // when
        val footballLeagueTeam = footballLeagueTeamRepository.getFootballLeagueTeamById(footballLeagueTeamId = 1).requireSuccess()

        // then
        Truth.assertThat(footballLeagueTeam).isEqualTo(
            FootballLeagueTeam(
                name = fakeFootballLeagueTeamDto.name,
                players = fakeFootballLeagueTeamDto.members.map {
                    FootballLeagueTeam.Player(name = it.name, position = requireNotNull(it.position))
                }
            )
        )
    }

    private companion object {
        val fakeFootballLeagueTeamDto = FootballTeamLeagueDto(
            name = "Liverpool FC",
            crestUrl = "https://crests.football-data.org/64.svg",
            members = listOf(
                FootballTeamLeagueDto.Member(
                    name = "Thiago Alcântara", position = "Midfielder", rawType = "PLAYER"
                ),
                FootballTeamLeagueDto.Member(
                    name = "Alisson", position = "Goalkeeper", rawType = "PLAYER"
                ),
                FootballTeamLeagueDto.Member(
                    name = "Roberto Firmino", position = "Attacker", rawType = "PLAYER"
                ),
                FootballTeamLeagueDto.Member(
                    name = "Jordan Henderson", position = "Midfielder", rawType = "PLAYER"
                ),
                FootballTeamLeagueDto.Member(
                    name = "Alex Oxlade-Chamberlain", position = "Midfielder", rawType = "PLAYER"
                ),
                FootballTeamLeagueDto.Member(
                    name = "Sadio Mané", position = "Attacker", rawType = "PLAYER"
                ),
                FootballTeamLeagueDto.Member(
                    name = "Divock Origi", position = "Attacker", rawType = "PLAYER"
                ),
                FootballTeamLeagueDto.Member(
                    name = "Mohamed Salah", position = "Attacker", rawType = "PLAYER"
                ),
                FootballTeamLeagueDto.Member(
                    name = "Diogo Jota", position = "Attacker", rawType = "PLAYER"
                ),
                FootballTeamLeagueDto.Member(
                    name = "Kostas Tsimikasa", position = "Defender", rawType = "PLAYER"
                ),
                FootballTeamLeagueDto.Member(
                    name = "James Milner", position = "Midfielder", rawType = "PLAYER"
                ),
                FootballTeamLeagueDto.Member(
                    name = "Joe Gomez", position = "Defender", rawType = "PLAYER"
                )
            )
        )
    }
}
