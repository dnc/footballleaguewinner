package com.example.footballleaguewinner.data.football.league

import com.example.footballleaguewinner.domain.football.league.FootballLeague
import com.example.footballleaguewinner.domain.football.league.FootballLeagueMatches
import com.google.common.truth.Truth
import com.squareup.moshi.Moshi
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import java.time.LocalDate

class FootballLeagueRepositoryImplTest {
    @Test
    fun whenReturningFootballLeagueById_shouldMapCorrectly() = runBlocking<Unit> {
        // given
        val footballLeagueService = Mockito.mock(FootballLeagueService::class.java)
        Mockito.`when`(footballLeagueService.getLeagueById(footballLeagueId = ArgumentMatchers.anyLong())).thenReturn(
            fakeFootballLeagueDto
        )

        val footballLeagueTeamRepository = FootballLeagueRepositoryImpl(
            moshi = Moshi.Builder().build(), // Cannot be mocked via Mockito as it's a final class
            footballLeagueService = footballLeagueService
        )

        // when
        val footballLeague = footballLeagueTeamRepository.getLeagueById(footballLeagueId = 1).requireSuccess()

        // then
        Truth.assertThat(footballLeague).isEqualTo(
            FootballLeague(
                name = fakeFootballLeagueDto.name,
                emblemUrl = fakeFootballLeagueDto.emblemUrl
            )
        )
    }

    @Test
    fun whenReturningFinishedMatchesByLeagueId_shouldMapCorrectly() = runBlocking<Unit> {
        // given
        val footballLeagueService = Mockito.mock(FootballLeagueService::class.java)
        Mockito.`when`(
            footballLeagueService.getFinishedMatchesByLeagueId(
                footballLeagueId = ArgumentMatchers.anyLong(),
                matchStartDate = ArgumentMatchers.any(String::class.java),
                matchEndDate = ArgumentMatchers.any(String::class.java)
            )
        ).thenReturn(fakeFootballLeagueMatchesDto)

        val footballLeagueTeamRepository = FootballLeagueRepositoryImpl(
            moshi = Moshi.Builder().build(), // Cannot be mocked via Mockito as it's a final class
            footballLeagueService = footballLeagueService
        )

        // when
        val footballLeagueMatches = footballLeagueTeamRepository.getFinishedMatchesByLeagueId(footballLeagueId = 1, matchStartDate = LocalDate.now(), matchEndDate = LocalDate.now().minusDays(30)).requireSuccess()

        // then
        Truth.assertThat(footballLeagueMatches).isEqualTo(
            FootballLeagueMatches(
                leagueName = fakeFootballLeagueMatchesDto.league.name,
                matches = fakeFootballLeagueMatchesDto.matches.map { matchDto ->
                    FootballLeagueMatches.Match(
                        time = matchDto.time,
                        winningTeam = when (matchDto.winnerType) {
                            FootballLeagueMatchesDto.MatchDto.WinnerType.HOME_TEAM -> matchDto.homeTeam
                            FootballLeagueMatchesDto.MatchDto.WinnerType.AWAY_TEAM -> matchDto.awayTeam
                            else -> error("Not supported")
                        }.run { FootballLeagueMatches.Match.Team(id = id, name = name) }
                    )
                }
            )
        )
    }

    @Test
    fun whenReturningLastFinishedMatchInLatestSeasonByLeagueId_shouldWorkCorrectly() = runBlocking<Unit> {
        // given
        val footballLeagueService = Mockito.mock(FootballLeagueService::class.java)
        Mockito.`when`(
            footballLeagueService.getFinishedMatchesByLeagueId(
                footballLeagueId = ArgumentMatchers.anyLong(),
                matchStartDate = ArgumentMatchers.isNull(),
                matchEndDate = ArgumentMatchers.isNull()
            )
        ).thenReturn(fakeFootballLeagueMatchesDto)

        val footballLeagueTeamRepository = FootballLeagueRepositoryImpl(
            moshi = Moshi.Builder().build(), // Cannot be mocked via Mockito as it's a final class
            footballLeagueService = footballLeagueService
        )

        // when
        val lastFinishedMatchInLatestSeason = footballLeagueTeamRepository.getLastFinishedMatchInLatestSeasonByLeagueId(footballLeagueId = 1).requireSuccess()

        // then
        val serviceReturnedMatch = fakeFootballLeagueMatchesDto.matches[1]
        Truth.assertThat(lastFinishedMatchInLatestSeason).isEqualTo(
            FootballLeagueMatches.Match(
                time = serviceReturnedMatch.time,
                winningTeam = when (serviceReturnedMatch.winnerType) {
                    FootballLeagueMatchesDto.MatchDto.WinnerType.HOME_TEAM -> serviceReturnedMatch.homeTeam
                    FootballLeagueMatchesDto.MatchDto.WinnerType.AWAY_TEAM -> serviceReturnedMatch.awayTeam
                    else -> error("Not supported")
                }.run { FootballLeagueMatches.Match.Team(id = id, name = name) }
            )
        )
    }

    private companion object {
        val fakeFootballLeagueDto = FootballLeagueDto(
            name = "UEFA Champions League",
            emblemUrl = "https://upload.wikimedia.org/wikipedia/en/b/bf/UEFA_Champions_League_logo_2.svg"
        )

        val fakeFootballLeagueMatchesDto = FootballLeagueMatchesDto(
            league = FootballLeagueMatchesDto.LeagueDto(id = 1, name = "UEFA Champions League"),
            matches = listOf(
                FootballLeagueMatchesDto.MatchDto(
                    awayTeam = FootballLeagueMatchesDto.MatchDto.TeamDto(id = 1, name = "Liverpool FC"),
                    homeTeam = FootballLeagueMatchesDto.MatchDto.TeamDto(id = 2, name = "Real Madrid"),
                    utcDate = "2021-03-06T09:30:07Z",
                    score = FootballLeagueMatchesDto.MatchDto.ScoreDto(rawWinnerType = "AWAY_TEAM")
                ),
                FootballLeagueMatchesDto.MatchDto(
                    awayTeam = FootballLeagueMatchesDto.MatchDto.TeamDto(id = 1, name = "Liverpool FC"),
                    homeTeam = FootballLeagueMatchesDto.MatchDto.TeamDto(id = 2, name = "Real Madrid"),
                    utcDate = "2021-03-07T09:30:07Z",
                    score = FootballLeagueMatchesDto.MatchDto.ScoreDto(rawWinnerType = "HOME_TEAM")
                )
            )
        )
    }
}
