package com.example.footballleaguewinner

import android.app.Application
import androidx.test.espresso.idling.CountingIdlingResource
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class FootballLeagueWinnerApplication : Application() {
    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        setUpTimber()
    }

    private fun setUpTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Timber.d("Initialized Timber")
        }
    }

    companion object {
        lateinit var instance: Application private set
        val countingIdlingResource: CountingIdlingResource by lazy { CountingIdlingResource("counting_idling_resource") }
    }
}
