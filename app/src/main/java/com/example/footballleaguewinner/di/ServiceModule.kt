package com.example.footballleaguewinner.di

import androidx.test.espresso.idling.CountingIdlingResource
import com.example.footballleaguewinner.FootballLeagueWinnerApplication
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {
    @Provides
    fun provideCountingIdlingResource(): CountingIdlingResource = FootballLeagueWinnerApplication.countingIdlingResource
}
