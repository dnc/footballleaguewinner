package com.example.footballleaguewinner.ui.features.league.team

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.footballleaguewinner.R
import com.example.footballleaguewinner.contract.fragment.BaseFragment
import com.example.footballleaguewinner.databinding.TopLeagueTeamFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TopLeagueTeamFragment : BaseFragment() {
    override val viewModel by viewModels<TopLeagueTeamViewModel>()

    private var binding: TopLeagueTeamFragmentBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return TopLeagueTeamFragmentBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            model = viewModel
        }.also { binding = it }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(requireNotNull(binding)) {
            listTopTeam.layoutManager = if (requireContext().resources.getBoolean(R.bool.isTablet)) {
                GridLayoutManager(requireContext(), 2)
            } else {
                LinearLayoutManager(requireContext())
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
