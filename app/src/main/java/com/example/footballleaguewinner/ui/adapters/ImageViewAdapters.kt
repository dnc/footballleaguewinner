package com.example.footballleaguewinner.ui.adapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.ImageLoader
import coil.decode.SvgDecoder
import coil.load
import com.example.footballleaguewinner.R
import timber.log.Timber

private lateinit var imageLoader: ImageLoader

@BindingAdapter("imageUrl")
fun ImageView.loadImageFromUrl(imageUrl: String?) {
    imageUrl ?: return
    Timber.d("Loading image from '$imageUrl'")

    // TODO: inject via DI
    if (!::imageLoader.isInitialized) {
        imageLoader = ImageLoader.Builder(context)
            .componentRegistry {
                add(SvgDecoder(context))
            }
            .crossfade(true)
            .error(R.drawable.ic_baseline_broken_image_24)
            .placeholder(R.drawable.ic_baseline_sports_soccer_24)
            .build()
    }

    this.load(uri = imageUrl, imageLoader = imageLoader)
}
