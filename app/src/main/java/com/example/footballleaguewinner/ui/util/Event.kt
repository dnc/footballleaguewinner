package com.example.footballleaguewinner.ui.util

/**
 * A generic event that may or may not have been handled
 */
class Event<out T>(private val content: T) {
    private var isConsumed = false

    fun consume(): T? {
        return if (isConsumed) {
            null
        } else {
            isConsumed = true
            content
        }
    }

    fun peekContent(): T = content
}
