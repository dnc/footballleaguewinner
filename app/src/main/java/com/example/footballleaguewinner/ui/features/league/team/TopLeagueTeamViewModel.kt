package com.example.footballleaguewinner.ui.features.league.team

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.test.espresso.idling.CountingIdlingResource
import com.example.footballleaguewinner.R
import com.example.footballleaguewinner.contract.viewmodel.BaseViewModel
import com.example.footballleaguewinner.domain.EntityId
import com.example.footballleaguewinner.domain.football.league.FootballLeague
import com.example.footballleaguewinner.domain.football.league.FootballLeagueMatches
import com.example.footballleaguewinner.domain.football.league.match.FootballLeagueRepository
import com.example.footballleaguewinner.domain.football.league.team.FootballLeagueTeam
import com.example.footballleaguewinner.domain.football.league.team.FootballLeagueTeamRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.launch
import me.tatarka.bindingcollectionadapter2.BR
import me.tatarka.bindingcollectionadapter2.ItemBinding
import timber.log.Timber
import java.time.LocalDate
import javax.inject.Inject

@HiltViewModel
@SuppressLint("StaticFieldLeak")
class TopLeagueTeamViewModel @Inject constructor(
    @ApplicationContext private val context: Context,
    private val countingIdlingResource: CountingIdlingResource,
    private val footballLeagueRepository: FootballLeagueRepository,
    private val footballLeagueTeamRepository: FootballLeagueTeamRepository
) : BaseViewModel() {
    private val _footballLeague = MutableLiveData<FootballLeague>()
    val footballLeague: LiveData<FootballLeague> get() = _footballLeague

    private val _topFootballLeagueTeam = MutableLiveData<FootballLeagueTeam>()
    val topFootballLeagueTeam: LiveData<FootballLeagueTeam> get() = _topFootballLeagueTeam

    val footballLeagueTeamPlayerItemBinding =
        ItemBinding.of<FootballLeagueTeam.Player>(BR.model, R.layout.top_league_team_player_item_view)

    private var initializationJob: Job? = null

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)

        initializationJob?.cancel()
        initializationJob = viewModelScope.launch {
            initialize()
        }
    }

    suspend fun initialize() {
        countingIdlingResource.increment()
        Timber.v("Incremented counter")

        loadFootballLeagueById(footballLeagueId = UEFA_CHAMPIONS_LEAGUE_ID)

        Timber.d("Loading matches from the past 30 days")
        val todayDate = LocalDate.now()

        loadFootballMatches(
            matchStartDate = todayDate.minusDays(30),
            matchEndDate = todayDate,
            onSuccess = {
                val topWinningTeam = it.getTopWinningTeamOrNull()

                if (topWinningTeam != null) {
                    // Happy path: we were able to extract the winning team from matches from the past 30 days
                    Timber.d("Found top winning team based on matches from the past 30 days")
                    loadTopFootballLeagueTeamById(topWinningTeamId = topWinningTeam.id)

                    return@loadFootballMatches
                }

                Timber.w("Cannot find top winning team from matches from the past 30 days")

                Timber.d("Loading last match from the latest season")

                // Load last match from latest season, to use its date as the end date for fetching the final match list
                loadLastMatchInLatestSeason(
                    onSuccess = { lastMatchInLatestSeason ->
                        if (lastMatchInLatestSeason != null) {
                            Timber.d("Last match in latest season occurred on ${lastMatchInLatestSeason.time}")

                            val lastMatchDate = lastMatchInLatestSeason.time.toLocalDate()
                            Timber.d("Loading matches from the $lastMatchDate")

                            loadFootballMatches(
                                matchStartDate = lastMatchDate.minusDays(30),
                                matchEndDate = lastMatchDate,
                                onSuccess = { footballLeagueMatches ->
                                    val topWinningTeam = footballLeagueMatches.getTopWinningTeamOrNull()

                                    if (topWinningTeam != null) {
                                        Timber.d("Found top winning team")
                                        loadTopFootballLeagueTeamById(topWinningTeamId = topWinningTeam.id)
                                    } else {
                                        // TODO: multiple error messages explaining issue: empty match list, no matches have winners (draw), etc.
                                        Timber.w("Cannot find top winning team")
                                        emitErrorMessage(context.getString(R.string.top_league_team_failed_top_team_search_message))
                                    }
                                }
                            )
                        } else {
                            // TODO: multiple error messages explaining issue: league has no seasons, no matches in season, etc.
                            Timber.w("Cannot find matches in latest season. Cannot load winning team")
                            emitErrorMessage(context.getString(R.string.top_league_team_failed_last_match_in_latest_season_load_message))
                        }
                    }
                )
            }
        )

        countingIdlingResource.decrement()
        Timber.v("Decremented counter")
    }

    private suspend fun loadFootballLeagueById(footballLeagueId: EntityId) {
        loadResource(
            description = "football league",
            source = {
                footballLeagueRepository.getLeagueById(footballLeagueId = footballLeagueId)
            },
            onSuccess = { _footballLeague.value = it }
        )
    }

    private suspend fun loadFootballMatches(
        matchStartDate: LocalDate,
        matchEndDate: LocalDate,
        onSuccess: suspend (FootballLeagueMatches) -> Unit
    ) {
        loadResource(
            description = "matches from $matchStartDate to $matchEndDate",
            source = {
                footballLeagueRepository.getFinishedMatchesByLeagueId(
                    footballLeagueId = UEFA_CHAMPIONS_LEAGUE_ID,
                    matchStartDate,
                    matchEndDate
                )
            },
            onSuccess = onSuccess
        )
    }

    /**
     * Gets the [team][FootballLeagueMatches.Match.Team] that won most of the matches, or null, if the
     * [matches][FootballLeagueMatches.matches] list is empty
     */
    private fun FootballLeagueMatches.getTopWinningTeamOrNull(): FootballLeagueMatches.Match.Team? {
        return matches.groupingBy { it.winningTeam }.eachCount().maxByOrNull { it.value }?.key.also {
            if (it == null) {
                Timber.w("Winning team is null")
            } else {
                Timber.d("Winning team is '${it.name}'")
            }
        }
    }

    private suspend fun loadTopFootballLeagueTeamById(topWinningTeamId: EntityId) {
        loadResource(
            description = "top winning team",
            source = {
                footballLeagueTeamRepository.getFootballLeagueTeamById(
                    footballLeagueTeamId = topWinningTeamId
                )
            },
            onSuccess = {
                _topFootballLeagueTeam.value = it
                emitSuccessMessage(context.getString(R.string.top_league_team_successful_top_team_load_message, it.name))
            }
        )
    }

    private suspend fun loadLastMatchInLatestSeason(onSuccess: suspend (FootballLeagueMatches.Match?) -> Unit) {
        loadResource(
            description = "last match in latest season",
            source = {
                footballLeagueRepository.getLastFinishedMatchInLatestSeasonByLeagueId(
                    footballLeagueId = UEFA_CHAMPIONS_LEAGUE_ID
                )
            },
            onSuccess = onSuccess
        )
    }

    private companion object {
        // At the time of writing this, "UEFA Champions League" has an active season in the past 30 days.
        const val UEFA_CHAMPIONS_LEAGUE_ID = 2001L
    }
}
