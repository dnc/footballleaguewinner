package com.example.footballleaguewinner.contract.viewmodel

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.footballleaguewinner.FootballLeagueWinnerApplication
import com.example.footballleaguewinner.R
import com.example.footballleaguewinner.domain.GenericFailureResponse
import com.example.footballleaguewinner.domain.Resource
import com.example.footballleaguewinner.domain.UnknownFailureResponse
import com.example.footballleaguewinner.ui.util.Event
import timber.log.Timber

abstract class BaseViewModel : ViewModel(), DefaultLifecycleObserver {
    private val unknownErrorMessage by lazy {
        // TODO: inject context
        FootballLeagueWinnerApplication.instance.getString(R.string.general_unknown_failure_message)
    }

    private val _successMessage = MutableLiveData<Event<String>>()
    val successMessage: LiveData<Event<String>> get() = _successMessage

    private val _errorMessage = MutableLiveData<Event<String>>()
    val errorMessage: LiveData<Event<String>> get() = _errorMessage

    protected fun emitSuccessMessage(message: String) {
        _successMessage.value = Event(message)
    }

    protected fun emitErrorMessage(message: String) {
        _errorMessage.value = Event(message)
    }

    protected suspend fun <T : Any?> loadResource(
        description: String,
        source: suspend () -> Resource<T>,
        onSuccess: suspend (T) -> Unit = {},
        onFailure: suspend () -> Unit = {}
    ): Resource<T> {
        Timber.d("Loading '$description'")
        // TODO: show loading overlay / block user input

        val resource = source()

        resource.handle(
            taskDescription = description, onSuccess = onSuccess, onFailure = onFailure
        )

        // TODO: hide loading overlay / unblock user input
        return resource
    }

    private suspend fun <T : Any?> Resource<T>.handle(
        taskDescription: String,
        onSuccess: suspend (T) -> Unit,
        onFailure: suspend () -> Unit
    ) {
        val resource = this

        when {
            resource.isSuccessful -> {
                Timber.d("Task '$taskDescription' success")
                onSuccess(resource.requireSuccess())
            }
            resource.isFailed -> {
                val failure = resource.requireFailure()
                when (failure) {
                    is GenericFailureResponse -> { _errorMessage.value = Event(requireNotNull(failure.reason)) }
                    is UnknownFailureResponse -> { _errorMessage.value = Event(requireNotNull(failure.reason?.message ?: unknownErrorMessage)) }
                }
                Timber.e("Task '$taskDescription' failure: (${failure.reason})")
                onFailure()
            }
        }
    }
}
