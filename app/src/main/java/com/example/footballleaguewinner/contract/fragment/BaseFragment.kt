package com.example.footballleaguewinner.contract.fragment

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.footballleaguewinner.contract.viewmodel.BaseViewModel
import com.example.footballleaguewinner.ui.extensions.observeValue

abstract class BaseFragment : Fragment() {
    protected abstract val viewModel: BaseViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.successMessage.observeValue(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }

        viewModel.errorMessage.observeValue(viewLifecycleOwner) {
            // TODO: use different style for errors
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }
    }
}
