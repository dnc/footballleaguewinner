package com.example.footballleaguewinner.ui.features.league.team

import com.example.footballleaguewinner.domain.EntityId
import com.example.footballleaguewinner.domain.Resource
import com.example.footballleaguewinner.domain.football.league.team.FootballLeagueTeam
import com.example.footballleaguewinner.domain.football.league.team.FootballLeagueTeamRepository

open class FakeFootballLeagueTeamRepository : FootballLeagueTeamRepository {
    override suspend fun getFootballLeagueTeamById(footballLeagueTeamId: EntityId): Resource<FootballLeagueTeam> {
        return Resource.success(
            FootballLeagueTeam(
                name = "Liverpool FC",
                players = listOf(
                    FootballLeagueTeam.Player(
                        name = "Thiago Alcântara", position = "Midfielder"
                    ),
                    FootballLeagueTeam.Player(
                        name = "Alisson", position = "Goalkeeper"
                    ),
                    FootballLeagueTeam.Player(
                        name = "Roberto Firmino", position = "Attacker"
                    ),
                    FootballLeagueTeam.Player(
                        name = "Jordan Henderson", position = "Midfielder"
                    ),
                    FootballLeagueTeam.Player(
                        name = "Alex Oxlade-Chamberlain", position = "Midfielder"
                    ),
                    FootballLeagueTeam.Player(
                        name = "Sadio Mané", position = "Attacker"
                    ),
                    FootballLeagueTeam.Player(
                        name = "Divock Origi", position = "Attacker"
                    ),
                    FootballLeagueTeam.Player(
                        name = "Mohamed Salah", position = "Attacker"
                    ),
                    FootballLeagueTeam.Player(
                        name = "Diogo Jota", position = "Attacker"
                    ),
                    FootballLeagueTeam.Player(
                        name = "Kostas Tsimikasa", position = "Defender"
                    ),
                    FootballLeagueTeam.Player(
                        name = "James Milner", position = "Midfielder"
                    ),
                    FootballLeagueTeam.Player(
                        name = "Joe Gomez", position = "Defender"
                    )
                )
            )
        )
    }
}
