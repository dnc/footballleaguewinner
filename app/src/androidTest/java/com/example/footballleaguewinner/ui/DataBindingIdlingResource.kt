/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.footballleaguewinner.ui

import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.IdlingResource
import java.util.UUID

/**
 * An espresso idling resource implementation that reports idle status for all data binding
 * layouts from all fragments. Data Binding uses a mechanism to post messages which Espresso doesn't track yet.
 *
 * Since this application runs UI tests at the fragment layer, this relies on implementations
 * calling [monitorActivity] with a [ActivityScenario], thereby monitoring all bindings in that
 * activity's fragments and any of their child views.
 *
 * Adapted from fragment based implementation:  https://github.com/android/architecture-components-samples
 */
class DataBindingIdlingResource : IdlingResource {
    private val idlingCallbacks = mutableListOf<IdlingResource.ResourceCallback>()

    /*
    Give it a unique id to workaround an espresso bug where you cannot register/unregister
    an idling resource w/ the same name
    */
    private val id = UUID.randomUUID().toString()

    /*
    Holds whether isIdle is called and the result was false. We track this to avoid calling
    onTransitionToIdle callbacks if Espresso never thought we were idle in the first place
    */
    private var wasNotIdle = false

    private lateinit var scenario: ActivityScenario<out AppCompatActivity>

    override fun getName() = "DataBinding $id"

    fun monitorActivity(activityScenario: ActivityScenario<out AppCompatActivity>) {
        scenario = activityScenario
    }

    override fun isIdleNow(): Boolean {
        val isIdle = !getBindings().any { it.hasPendingBindings() }

        if (isIdle) {
            if (wasNotIdle) {
                // Notify observers to avoid espresso race detector
                idlingCallbacks.forEach { it.onTransitionToIdle() }
            }
            wasNotIdle = false
        } else {
            wasNotIdle = true

            // Check next frame
            scenario.onActivity { activity ->
                activity.findViewById<View>(android.R.id.content).postDelayed(::isIdleNow, 16)
            }
        }
        return isIdle
    }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback) {
        idlingCallbacks.add(callback)
    }

    /**
     * Find all binding classes in all currently available fragments.
     */
    private fun getBindings(): List<ViewDataBinding> {
        lateinit var bindings: List<ViewDataBinding>

        scenario.onActivity { activity ->
            bindings = activity.supportFragmentManager.fragments.map {
                it.requireView().flattenHierarchy().mapNotNull { view ->
                    DataBindingUtil.getBinding<ViewDataBinding>(view)
                }
            }.flatten()
        }

        return bindings
    }

    private fun View.flattenHierarchy(): List<View> = if (this is ViewGroup) {
        listOf(this) + children.map { it.flattenHierarchy() }.flatten()
    } else {
        listOf(this)
    }
}
