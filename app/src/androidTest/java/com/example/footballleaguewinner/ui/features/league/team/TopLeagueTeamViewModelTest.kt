package com.example.footballleaguewinner.ui.features.league.team

import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.idling.CountingIdlingResource
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import java.time.LocalDate

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class TopLeagueTeamViewModelTest {
    @Test
    fun whenInitialized_shouldInteractWithRepositories() = runBlocking<Unit> {
        // given
        val fakeFootballLeagueRepository = Mockito.spy(FakeFootballLeagueRepository())
        val fakeFootballLeagueTeamRepository = Mockito.spy(FakeFootballLeagueTeamRepository())

        val viewModel = withContext(Dispatchers.Main) {
            TopLeagueTeamViewModel(
                context = ApplicationProvider.getApplicationContext(),
                countingIdlingResource = CountingIdlingResource("counting_idling_resource"),
                footballLeagueRepository = fakeFootballLeagueRepository,
                footballLeagueTeamRepository = fakeFootballLeagueTeamRepository
            )
        }

        // when
        withContext(Dispatchers.Main) { viewModel.initialize() }

        // then
        // Ensure league info is requested
        Mockito.verify(fakeFootballLeagueRepository).getLeagueById(UEFA_CHAMPIONS_LEAGUE_ID)

        // Ensure league match info is requested
        Mockito.verify(fakeFootballLeagueRepository).getFinishedMatchesByLeagueId(
            UEFA_CHAMPIONS_LEAGUE_ID, LocalDate.now().minusDays(30), LocalDate.now()
        )

        // Ensure winning team is requested
        Mockito.verify(fakeFootballLeagueTeamRepository).getFootballLeagueTeamById(LIVERPOOL_FC_ID)
    }

    @Test
    fun whenInitialized_shouldLoadCorrectDataForUefaChampionsLeague() = runBlocking<Unit> {
        // TODO: observe LiveData objects to ensure the right data is actually loaded
        // See: https://developer.android.com/codelabs/advanced-android-kotlin-training-testing-basics#8
    }

    @Test
    fun otherTests() {
        // TODO
    }

    private companion object {
        const val UEFA_CHAMPIONS_LEAGUE_ID = 2001L
        const val LIVERPOOL_FC_ID = 64L
    }
}
