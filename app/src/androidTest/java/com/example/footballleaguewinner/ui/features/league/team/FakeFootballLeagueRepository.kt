package com.example.footballleaguewinner.ui.features.league.team

import com.example.footballleaguewinner.domain.EntityId
import com.example.footballleaguewinner.domain.Resource
import com.example.footballleaguewinner.domain.football.league.FootballLeague
import com.example.footballleaguewinner.domain.football.league.FootballLeagueMatches
import com.example.footballleaguewinner.domain.football.league.match.FootballLeagueRepository
import java.time.LocalDate
import java.time.LocalDateTime

open class FakeFootballLeagueRepository : FootballLeagueRepository {
    override suspend fun getLeagueById(footballLeagueId: EntityId): Resource<FootballLeague> {
        return Resource.success(FootballLeague("UEFA Champions League", "https://upload.wikimedia.org/wikipedia/en/b/bf/UEFA_Champions_League_logo_2.svg"))
    }

    override suspend fun getFinishedMatchesByLeagueId(
        footballLeagueId: EntityId,
        matchStartDate: LocalDate,
        matchEndDate: LocalDate
    ): Resource<FootballLeagueMatches> {
        return Resource.success(
            FootballLeagueMatches(
                leagueName = "UEFA Champions League",
                matches = listOf(
                    FootballLeagueMatches.Match(
                        time = LocalDateTime.now(), winningTeam = FootballLeagueMatches.Match.Team(id = 64, name = "Liverpool FC")
                    )
                )
            )
        )
    }

    override suspend fun getLastFinishedMatchInLatestSeasonByLeagueId(footballLeagueId: EntityId): Resource<FootballLeagueMatches.Match?> {
        return Resource.success(
            FootballLeagueMatches.Match(
                time = LocalDateTime.now(), winningTeam = FootballLeagueMatches.Match.Team(id = 64, name = "Liverpool FC")
            )
        )
    }
}
