package com.example.footballleaguewinner.ui

import androidx.test.espresso.IdlingRegistry
import com.example.footballleaguewinner.FootballLeagueWinnerApplication
import org.junit.rules.TestWatcher
import org.junit.runner.Description

class CountingIdlingResourceRule : TestWatcher() {
    private val countingIdlingResource by lazy {
        FootballLeagueWinnerApplication.countingIdlingResource
    }

    override fun finished(description: Description?) {
        IdlingRegistry.getInstance().unregister(countingIdlingResource)
        super.finished(description)
    }

    override fun starting(description: Description?) {
        IdlingRegistry.getInstance().register(countingIdlingResource)
        super.starting(description)
    }
}
