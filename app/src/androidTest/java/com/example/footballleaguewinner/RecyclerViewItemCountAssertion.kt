package com.example.footballleaguewinner

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.Matcher

/**
 * A [ViewAssertion] implementation that asserts the number or items in a [RecyclerView]'s adapter
 */
class RecyclerViewItemCountAssertion private constructor(private val matcher: Matcher<Int>) : ViewAssertion {
    override fun check(view: View, noViewFoundException: NoMatchingViewException?) {
        if (noViewFoundException != null) {
            throw noViewFoundException
        }
        val recyclerView = view as RecyclerView
        val adapter = requireNotNull(recyclerView.adapter)
        assertThat(adapter.itemCount, matcher)
    }

    companion object {
        fun withItemCount(expectedItemCount: Int): RecyclerViewItemCountAssertion {
            return RecyclerViewItemCountAssertion(`is`(expectedItemCount))
        }
    }
}
