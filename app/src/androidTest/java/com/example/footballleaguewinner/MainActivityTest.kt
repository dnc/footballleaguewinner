package com.example.footballleaguewinner

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.footballleaguewinner.RecyclerViewItemCountAssertion.Companion.withItemCount
import com.example.footballleaguewinner.ui.CountingIdlingResourceRule
import com.example.footballleaguewinner.ui.DataBindingIdlingResourceRule
import com.example.footballleaguewinner.ui.MainActivity
import org.hamcrest.CoreMatchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    @get:Rule var activityScenarioRule = activityScenarioRule<MainActivity>()

    @get:Rule val countingIdlingResourceRule = CountingIdlingResourceRule()
    @get:Rule val dataBindingIdlingResourceRule = DataBindingIdlingResourceRule()

    @Test
    fun requiredFieldsAreVisible() {
        dataBindingIdlingResourceRule.monitorActivity(activityScenario = activityScenarioRule.scenario)

        onView(withId(R.id.textLeagueName)).check(matches(allOf(isDisplayed(), withText("UEFA Champions League"))))
        onView(withId(R.id.textTopTeamName)).check(matches(allOf(isDisplayed(), withText("FC Bayern München"))))

        with(onView(withId(R.id.listTopTeam))) {
            check(matches(isDisplayed()))
            check(withItemCount(33))
        }

        /*
         TODO: would be better if it checked for actual player names, not just for player count. That wouldn't currently
         work, as we use data binding for the RecyclerView (Espresso issue). Writing a RecyclerView adapter and
         giving up on binding-collection-adapter would help

          Also not very reliable, since it talks to a backend and match dates used to extract the winning team are
          dynamic
         */
    }
}
