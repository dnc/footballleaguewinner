- Info: Android app that displays the football team that won most matches, over a 30 day period

 - Used libraries:
	 - Coil
	 - Moshi
	 - Retrofit
	 - Binding Collection Adapter
	 - Coroutines (master branch, RxJava2 on master_rxjava2)
	 - Hilt
	 - Timber
	 - JUnit
	 - Truth
	 - Mockito
	 - Espresso

 - How to build and run app

	 1. Set your *sdk.dir* and *FOOTBALL_MANAGER_API_KEY* in *<project_directory>\local.properties*

	    eg.:

	    sdk.dir=C\:\\Users\\John\\AppData\\Local\\Android\\Sdk

	    FOOTBALL_MANAGER_API_KEY=xyz

	2. Execute `gradle assemble release` while in *<project_directory>*
	3. Install APK found under *<project_directory>\app\build\outputs\apk\release*

- Run unit tests: `gradle test`

- Run instrumentation tests: `gradle connectedAndroidTest`


- Download prebuilt binaries: https://gitlab.com/dnc/footballleaguewinner/-/releases
